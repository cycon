.SUFFIXES:
.SUFFIXES: .o .c

CFLAGS ?=
LDFLAGS ?=
PKG_CONFIG ?= pkg-config

PREFIX ?= /usr
BINDIR ?= $(PREFIX)/bin
SHAREDIR ?= $(PREFIX)/share
MANDIR ?= $(SHAREDIR)/man

CFLAGS += -std=c99 -D_POSIX_C_SOURCE=200809L -D_DEFAULT_SOURCE  -D_XOPEN_SOURCE

# libcurl (only for scraping some session-id over HTTPS; meh.)
CFLAGS += $(shell $(PKG_CONFIG) --cflags libcurl)
LDFLAGS += $(shell $(PKG_CONFIG) --libs libcurl)

# libwebsockets
CFLAGS += $(shell $(PKG_CONFIG) --cflags libwebsockets)
LDFLAGS += $(shell $(PKG_CONFIG) --libs libwebsockets)

# pthreads
CFLAGS += -lpthread
LDFLAGS += -lpthread

# termkey
CFLAGS += $(shell $(PKG_CONFIG) --cflags termkey)
LDFLAGS += $(shell $(PKG_CONFIG) --libs termkey)

# unibilium
CFLAGS += $(shell $(PKG_CONFIG) --cflags unibilium)
LDFLAGS += $(shell $(PKG_CONFIG) --libs unibilium)

# yajl
CFLAGS += $(shell $(PKG_CONFIG) --cflags yajl)
LDFLAGS += $(shell $(PKG_CONFIG) --libs yajl)

# Debug
# CFLAGS += -g -O0 -pedantic -Wall -Wextra -Werror -DWRITE_LOGS=1

default: all
all: cycon

%.o: %.c cycon.h macros.h
	$(CC) $(CFLAGS) -c -o $@ $<

cycon: main.o cytube.o launch.o state.o ui.o util.o
	$(CC) -o $@ $^ $(LDFLAGS)

.PHONY: clean
clean:
	find -name '*.o' -delete
	rm -f cycon

.PHONY: install
install: all
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -f cycon $(DESTDIR)$(BINDIR)/
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	cp -f cycon.1 $(DESTDIR)$(MANDIR)/man1/

.PHONY: uninstall
uninstall:
	cd $(DESTDIR)$(BINDIR) && rm -f cycon
	cd $(DESTDIR)$(MANDIR) && rm -f man1/cycon.1
