/*
 * Copyright (c) 2019, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

/* snprintf + malloc + sprintf */
char *
aprintf(const char *format, ...)
{
        char *mem = 0;
        va_list arglist;
        size_t len = 0;

        va_start(arglist, format);
        len = vsnprintf(0, 0, format, arglist);
        va_end(arglist);

        if (!(mem = malloc(len + 1))) {
                goto done;
        }

        va_start(arglist, format);
        vsprintf(mem, format, arglist);
        va_end(arglist);
done:

        return mem;
}

/* Calculate how much space we need to print a string */
int
strwidth(const char *s)
{
        if (!s) {
                return 0;
        }

        int width = 0;
        wchar_t wc = 0;
        mbstate_t mbs = { 0 };
        size_t left = strlen(s);
        int l = 0;
        size_t mbret = 0;

        while (*s) {
                mbret = mbrtowc(&wc, s, left, &mbs);

                if (mbret >= (size_t) -2) {
                        mbs = (mbstate_t) { 0 };
                        s++;
                        left--;
                        continue;
                }

                l = wcwidth(wc);
                s += mbret;

                if (l >= 0) {
                        width += l;
                }
        }

        return width;
}
