/*
 * Copyright (c) 2019, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <yajl/yajl_tree.h>

#include "cycon.h"
#include "macros.h"

#define xmin(a, b) ((a) < (b) ? (a) : (b))

/* For state while in all the yajl callbacks */
struct parse_state {
        /* */
        int depth;
        struct cytube_message *m;
};

/* Various paths for accessing JSON */
const char *changemedia_id_path[] = { "id", 0 };
const char *changemedia_title_path[] = { "title", 0 };
const char *changemedia_runtime_path[] = { "duration", 0 };
const char *changemedia_seconds_path[] = { "seconds", 0 };
const char *changemedia_type_path[] = { "type", 0 };
const char *changemedia_currenttime_path[] = { "currentTime", 0 };
const char *changemedia_paused_path[] = { "paused", 0 };
const char *chatmsg_msg_path[] = { "msg", 0 };
const char *chatmsg_username_path[] = { "username", 0 };
const char *movemedia_from_path[] = { "from", 0 };
const char *movemedia_after_path[] = { "after", 0 };
const char *playlist_id_path[] = { "media", "id", 0 };
const char *playlist_runtime_path[] = { "media", "duration", 0 };
const char *playlist_title_path[] = { "media", "title", 0 };
const char *playlist_type_path[] = { "media", "type", 0 };
const char *playlist_uid_path[] = { "uid", 0 };
const char *queue_id_path[] = { "item", "media", "id", 0 };
const char *queue_runtime_path[] = { "item", "media", "duration", 0 };
const char *queue_title_path[] = { "item", "media", "title", 0 };
const char *queue_type_path[] = { "item", "media", "type", 0 };
const char *queue_uid_path[] = { "item", "uid", 0 };
const char *queue_after_path[] = { "after", 0 };

/* Fw decl */
static char * make_uri(const char *type, const char *id);

/* Add a message to the end of s's messages, maybe rolling over */
#define CHAT_LOG_LEN 70
static void
append_chat_entry(struct state volatile *s, const char *username, const
                  char *msg, int special)
{
        char *duptext = 0;
        size_t mlen = msg ? strlen(msg) : 0;

        if (s->chat_log.msgs_len >= CHAT_LOG_LEN) {
                /* Just slide them over */
                char *old_user = s->chat_log.msgs[0].user;
                char *old_text = s->chat_log.msgs[0].text;
                char *new_text = 0;

                memmove(s->chat_log.msgs, s->chat_log.msgs + 1, (CHAT_LOG_LEN -
                                                                 1) *
                        sizeof(*s->chat_log.msgs));
                s->chat_log.msgs[CHAT_LOG_LEN - 1] = (struct chat_msg) { /* */
                        .user = strdup(username), .text = strdup(msg),
                        .special = special
                };
                duptext = s->chat_log.msgs[CHAT_LOG_LEN - 1].text;
                mlen = new_text ? strlen(new_text) : 0;
                free(old_user);
                free(old_text);
        } else {
                size_t new_msgs_len = xmin(s->chat_log.msgs_len + 1,
                                           CHAT_LOG_LEN);
                struct chat_msg *new_msgs = calloc(sizeof(*new_msgs),
                                                   new_msgs_len);
                struct chat_msg *old_msgs = s->chat_log.msgs;
                size_t k = new_msgs_len - 1;

                if (!new_msgs) {
                        goto done;
                }

                new_msgs[k] = (struct chat_msg) { /* */
                        .user = strdup(username), .text = strdup(msg),
                        .special = special
                };
                duptext = new_msgs[k].text;
                memcpy(new_msgs, old_msgs, s->chat_log.msgs_len *
                       sizeof(*s->chat_log.msgs));
                s->chat_log.msgs = new_msgs;
                s->chat_log.msgs_len++;
                free(old_msgs);
        }

        for (size_t j = 0; duptext &&
             j < mlen; ++j) {
                if ((unsigned char) duptext[j] < 0x20) {
                        duptext[j] = ' ';
                }
        }

done:

        return;
}

/* Make sure a certain entry is in s's playlist */
static void
ensure_playlist_entry(struct state volatile *s, const char *id, const
                      char *title, const char *runtime, const char *type, int
                      maybe_after_uid, int
                      maybe_uid, int *out_uid)
{
        char *uri = make_uri(type, id);
        int last_uid = -1;
        int uid = maybe_uid;
        size_t where_this_entry_is = (size_t) -1;
        size_t where_prev_entry_is = (size_t) -1;
        char *duptitle = 0;

        /*
         * What we'd like to do is use the UIDs to insert into the
         * playlist. However, we might not have a UID. In that case,
         * fall back to the uri.
         */
        if (uid < 0) {
                /*
                 * This is bogus -- they don't give us a UID, but
                 * they might move it around later based on the
                 * UID, etc. Fixing this properly takes way too
                 * much computation, so let's just give this a fake
                 * UID. If the playlist actually matters, the next
                 * playlist ping will pick it up correctly within
                 * 2 minutes. If the playlist doesn't matter, then
                 * it's probably hidden and we'll just get a list
                 * of currently-playing stuff.
                 */
                for (size_t k = 0; k < s->playlist.entries_len; ++k) {
                        struct playlist_entry *e = &s->playlist.entries[k];

                        last_uid = e->uid;

                        if (!strcmp(e->uri, uri)) {
                                uid = e->uid;
                                break;
                        }
                }

                if (uid < 0) {
                        uid = last_uid + 10;
                }

        }

        /*
         * Now, just try and stick this thing in place. If there's
         * already an entry with this UID, we need to updated it
         * and move it to the right place (if maybe_after_uid is
         * set)
         */
        for (size_t k = 0; k < s->playlist.entries_len; ++k) {
                struct playlist_entry *e = &s->playlist.entries[k];

                if (e->uid == uid) {
                        where_this_entry_is = k;
                }

                if (e->uid == maybe_after_uid) {
                        where_prev_entry_is = k;
                }
        }


        if (where_prev_entry_is == (size_t) -1 &&
            s->playlist.entries_len > 0 &&
            where_this_entry_is != 0) {
                where_prev_entry_is = s->playlist.entries_len - 1;
        }

        if (where_this_entry_is == (size_t) -1) {

                /* We have no spot for this entry. So insert it */
                void *newmem = 0;
                size_t new_size = (s->playlist.entries_len + 1) *
                                  sizeof *(s->playlist.entries);
                struct playlist_entry *new_e = 0;

                if (s->playlist.entries_len > ((size_t) -1) >> 2) {
                        ERROR_MESSAGE("overflow");
                        goto done;
                }

                if (!(newmem = realloc(s->playlist.entries, new_size))) {
                        PERROR_MESSAGE("realloc");
                        goto done;
                }

                s->playlist.entries = newmem;
                s->playlist.entries_len = s->playlist.entries_len + 1;
                new_e = &(s->playlist.entries[s->playlist.entries_len - 1]);
                new_e->title = strdup(title);
                duptitle = new_e->title;
                new_e->title_width = strwidth(title);
                new_e->runtime = strdup(runtime);
                new_e->uri = strdup(uri);
                new_e->uid = uid;
                where_this_entry_is = s->playlist.entries_len - 1;
        } else {
                /*
                 * We actually have a spot for this entry. Just
                 * overwrite it.
                 */
                struct playlist_entry *e =
                        &s->playlist.entries[where_this_entry_is];

                free(e->title);
                e->title = strdup(title);
                duptitle = e->title;
                e->title_width = strwidth(title);
                free(e->runtime);
                e->runtime = strdup(runtime);
                free(e->uri);
                e->uri = strdup(uri);
                e->uid = uid;
        }

        /*
         * At this point, s->p.e[where_this_entry_is] actually holds
         * the data we want. The only problem imight be that it
         * might not be directly after where_prev_entry_is.
         */
        if (maybe_after_uid < 0 ||
            where_prev_entry_is == (size_t) -1 ||
            where_prev_entry_is + 1 == where_this_entry_is) {
                goto done;
        }

        /* Now we actually need to rearrange things, sadly. */
        if (where_prev_entry_is + 1 < where_this_entry_is) {
                struct playlist_entry temp_e = (struct playlist_entry) { 0 };
                size_t to_move_num = where_this_entry_is -
                                     (where_prev_entry_is + 1);
                size_t to_move_len = to_move_num * sizeof *s->playlist.entries;

                memcpy(&temp_e, &(s->playlist.entries[where_this_entry_is]),
                       sizeof temp_e);
                memmove(&(s->playlist.entries[where_prev_entry_is + 2]),
                        &(s->playlist.entries[where_prev_entry_is + 1]),
                        to_move_len);
                memcpy(&(s->playlist.entries[where_prev_entry_is + 1]), &temp_e,
                       sizeof temp_e);
        } else {
                struct playlist_entry temp_e = (struct playlist_entry) { 0 };
                size_t to_move_num = where_prev_entry_is - where_this_entry_is;
                size_t to_move_len = to_move_num * sizeof *s->playlist.entries;

                memcpy(&temp_e, &(s->playlist.entries[where_this_entry_is]),
                       sizeof temp_e);
                memmove(&(s->playlist.entries[where_this_entry_is]),
                        &(s->playlist.entries[where_this_entry_is + 1]),
                        to_move_len);
                memcpy(&(s->playlist.entries[where_prev_entry_is]), &temp_e,
                       sizeof temp_e);
        }

done:
        if (duptitle) {
                for (size_t k = 0; duptitle[k]; ++k) {
                        if ((unsigned char) duptitle[k] < 0x20) {
                                duptitle[k] = ' ';
                        }
                }
        }

        *out_uid = uid;
        free(uri);

        return;
}

/* Free all memory associated with a chat_log */
static void
clean_chat_log(struct chat_log volatile *c)
{
        for (size_t k = 0; k < c->msgs_len; ++k) {
                free(c->msgs[k].user);
                free(c->msgs[k].text);
        }

        free(c->msgs);
        c->msgs = 0;
        c->msgs_len = 0;
}

/* Free all memory associated with a playlist */
static void
clean_playlist(struct playlist volatile *p)
{
        for (size_t k = 0; k < p->entries_len; ++k) {
                struct playlist_entry *e = &p->entries[k];

                free(e->title);
                free(e->runtime);
                free(e->uri);
        }

        free(p->entries);
        p->entries = 0;
        p->entries_len = 0;
}

/* yajl_tree_get, returning strings */
static char *
get_string_from(yajl_val root, const char **path)
{
        yajl_val v = yajl_tree_get(root, path, yajl_t_string);

        return YAJL_GET_STRING(v);
}

/* yajl_tree_get, returning ints */
static int
get_integer_from(yajl_val root, const char **path)
{
        yajl_val v = yajl_tree_get(root, path, yajl_t_number);

        if (YAJL_IS_INTEGER(v)) {
                return YAJL_GET_INTEGER(v);
        }

        return 0;
}

/* yajl_tree_get, returning floating-point */
static float
get_double_from(yajl_val root, const char **path)
{
        yajl_val v = yajl_tree_get(root, path, yajl_t_number);

        if (YAJL_IS_DOUBLE(v)) {
                return YAJL_GET_DOUBLE(v);
        }

        return 0;
}

/* "yt", "9NACc7DBRh0" -> {an actual URI} */
static char *
make_uri(const char *type, const char *id)
{
        if (!strcmp(type, "yt")) {
                return aprintf("https://youtube.com/watch?v=%s", id);
        } else if (!strcmp(type, "vi")) {
                return aprintf("https://vimeo.com/%s", id);
        } else if (!strcmp(type, "dm")) {
                return aprintf("https://dailymotion.com/video/%s", id);
        } else if (!strcmp(type, "sc")) {
                return strdup(id);
        } else if (!strcmp(type, "li")) {
                return aprintf("https://livestream.com/%s", id);
        } else if (!strcmp(type, "tw")) {
                return aprintf("https://twitch.tv/%s", id);
        } else if (!strcmp(type, "rt")) {
                return strdup(id);
        } else if (!strcmp(type, "im")) {
                return aprintf("https://imgur.com/a/%s", id);
        } else if (!strcmp(type, "us")) {
                return aprintf("https://ustream.tv/channel/%s", id);
        } else if (!strcmp(type, "gd")) {
                return aprintf("https://docs.google.com/file/d/%s", id);
        } else if (!strcmp(type, "fi")) {
                return strdup(id);
        } else if (!strcmp(type, "hb")) {
                return aprintf("https://www.smashcast.tv/%s", id);
        } else if (!strcmp(type, "hl")) {
                return strdup(id);
        } else if (!strcmp(type, "sb")) {
                return aprintf("https://www.streamable.com/%s", id);
        } else if (!strcmp(type, "tc")) {
                return aprintf("https://clips.twitch.tv/%s", id);
        } else if (!strcmp(type, "cm")) {
                return strdup(id);
        }

        return strdup("");
}

/* Make valgrind happy */
void
state_clean(struct state volatile *s)
{
        /*
         * Note this function only affects the "application state"
         * and "connection info" parts of state. The UI stuff is
         * handled in ui.c.
         */
        clean_playlist(&s->playlist);
        clean_chat_log(&s->chat_log);
        free(s->socket_host);
        free(s->sid);
        *s = (struct state) { 0 };
}

/* Handle a message. */
int
state_handle(struct state volatile *s, const char *msg, size_t len)
{
        /*
         * The boilerplate for passing errors back out of the LWS
         * thread isn't worth it, so just do nothing on parse errors.
         */
        int ret = -1;
        char eb[1024] = { 0 };
        char *null_terminated = 0;
        const char *command = 0;
        yajl_val tree = 0;

        /*
         * YAJL has some stupid memory leaks when seeing invalid
         * JSON. Pull request #168 is one, there's another in string
         * handling, etc. Let's try to block a few of them.
         */
        if (msg[len - 1] != ']') {
                goto done;
        }

        if (!(null_terminated = malloc(len + 1))) {
                goto done;
        }

        memcpy(null_terminated, msg, len);
        null_terminated[len] = 0;
        tree = yajl_tree_parse(null_terminated, eb, 1024);

        if ((ret = -!!eb[0])) {
                goto done;
        }

        if (!tree ||
            !YAJL_IS_ARRAY(tree) ||
            YAJL_GET_ARRAY(tree)->len < 1 ||
            !((command = YAJL_GET_STRING(YAJL_GET_ARRAY(tree)->values[0])))) {
                goto done;
        }

        if (!strcmp(command, "setPermissions")) {
                if (!s->playlist.entries_len) {
                        s->must_ask_for_playlist = 1;
                }
        } else if (!strcmp(command, "changeMedia") &&
                   YAJL_GET_ARRAY(tree)->len >= 2) {
                s->dirty_top = 1;
                yajl_val e = YAJL_GET_ARRAY(tree)->values[1];
                char *id = get_string_from(e, changemedia_id_path);
                char *title = get_string_from(e, changemedia_title_path);
                char *runtime = get_string_from(e, changemedia_runtime_path);
                char *type = get_string_from(e, changemedia_type_path);
                int runtime_s = get_integer_from(e, changemedia_seconds_path);
                double currenttime = get_double_from(e,
                                                     changemedia_currenttime_path);
                char *paused = get_string_from(e, changemedia_paused_path);

                s->playlist.paused = (paused &&
                                      !strcmp(paused, "true"));
                s->playlist.current_playtime = (int) currenttime;
                s->playlist.current_length = runtime_s;
                s->playlist.zero_clocktime = time(0);
                s->playlist.zero_playtime = s->playlist.current_playtime;
                int uid = 0;

                ensure_playlist_entry(s, id, title, runtime, type, -1, -1,
                                      &uid);
                s->playlist.current_playing_uid = uid;
                s->dirty_playlist = 1;
        } else if (!strcmp(command, "moveMedia") &&
                   YAJL_GET_ARRAY(tree)->len >= 2) {
                yajl_val e = YAJL_GET_ARRAY(tree)->values[1];
                char *from = get_string_from(e, movemedia_from_path);
                char *after = get_string_from(e, movemedia_after_path);

                /* TODO: handle this */
                LOG("moveMedia: \u00ab%s\u00bb", null_terminated);
                LOG("from  = \u00ab%s\u00bb", from);
                LOG("after = \u00ab%s\u00bb", after);
        } else if (!strcmp(command, "playlist") &&
                   YAJL_GET_ARRAY(tree)->len >= 2 &&
                   YAJL_IS_ARRAY(YAJL_GET_ARRAY(tree)->values[1])) {
                int last_uid = -1;
                size_t these_len = YAJL_GET_ARRAY(YAJL_GET_ARRAY(
                                                          tree)->values[1])->len;

                for (size_t k = 0; k < these_len; ++k) {
                        yajl_val e = YAJL_GET_ARRAY(YAJL_GET_ARRAY(
                                                            tree)->values[1])->
                                     values[k];
                        char *id = get_string_from(e, playlist_id_path);
                        char *title = get_string_from(e, playlist_title_path);
                        char *runtime = get_string_from(e,
                                                        playlist_runtime_path);
                        char *type = get_string_from(e, playlist_type_path);
                        int uid = get_integer_from(e, playlist_uid_path);

                        ensure_playlist_entry(s, id, title, runtime, type,
                                              last_uid, uid, &uid);
                        last_uid = uid;
                }

                s->dirty_top = 1;
                s->dirty_playlist = 1;
        } else if (!strcmp(command, "chatMsg") &&
                   YAJL_GET_ARRAY(tree)->len >= 2) {
                yajl_val e = YAJL_GET_ARRAY(tree)->values[1];
                char *username = get_string_from(e, chatmsg_username_path);
                char *msg = get_string_from(e, chatmsg_msg_path);

                append_chat_entry(s, username, msg, 0);

                if (!s->uri_or_chat) {
                        s->dirty_top = 1;
                }
        } else if (!strcmp(command, "queue") &&
                   YAJL_GET_ARRAY(tree)->len >= 2) {
                yajl_val e = YAJL_GET_ARRAY(tree)->values[1];
                char *id = get_string_from(e, queue_id_path);
                char *title = get_string_from(e, queue_title_path);
                char *runtime = get_string_from(e, queue_runtime_path);
                char *type = get_string_from(e, queue_type_path);
                int uid = get_integer_from(e, queue_uid_path);
                int after = get_integer_from(e, queue_after_path);

                ensure_playlist_entry(s, id, title, runtime, type, after, uid,
                                      &uid);
                s->dirty_playlist = 1;
        }

done:
        yajl_tree_free(tree);
        free(null_terminated);

        return ret;
}
