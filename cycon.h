/*
 * Copyright (c) 2019, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

/* FWdecl */
struct lws_client_connect_info;
struct TermKey;
struct unibi_term;

/* A thing that we can play */
struct playlist_entry {
        /* */
        char *title;     /* What to show in the list */
        int title_width; /* How many cells the title takes to print */
        char *uri;       /* How to get the bits */
        char *runtime;   /* How long we have to sit through this */
        int uid;         /* For being deleted, skipped to, queued after */
};

/* A list of playlist entries */
struct playlist {
        /* */
        struct playlist_entry *entries;
        size_t entries_len;
        int current_playing_uid;
        int paused;
        int current_playtime;
        int current_length;
        int zero_clocktime;
        int zero_playtime;
};

/* A thing said by a person */
struct chat_msg {
        /* */
        char *user;  /* Who said it */
        char *text;  /* What they said */
        int special; /* Whether this is really an announcement/poll/something */
};

/* Things said by people */
struct chat_log {
        /* */
        struct chat_msg *msgs;
        size_t msgs_len;
};

/* User data passed to callbacks: all state */

/*
   Note that there is NO locking, because I don't care quite enough
   to put it in, much less get it correct.
 */
struct state {
        /* An error message the server may have sent */
        char *server_error;

        /* connection info; const are from argv and should not be free()d */
        const char *address;
        const char *channel_name;
        int https;
        char *socket_host;         /* oh joy, there are multiple servers to talk to */
        /* Fucking layers on top of layers on top of insanity */
        char *stored_cmd;
        size_t stored_cmd_len;

        /* middleware stuff */
        char *sid;                        /* From socket.io's initialization */
        time_t ping_interval;             /* Ping this often */
        time_t last_ping;                 /* When we ping'd last */
        time_t last_playlist_req;         /* When we last requested playlist */
        int established;                  /* Have we seen LWS_CALLBACK_CLIENT_ESTABLISHED? */
        /* application state - domain of state.c */
        struct playlist playlist;         /* What the server tells us is queued */
        struct chat_log chat_log;         /* What the clowns have said */
        int please_die;                   /* Should we shut down */
        int must_write_ping;              /* Determining what to write */
        int must_write_upgrade;           /* ^ */
        int must_join_channel;            /* ^ */
        int must_ask_for_playlist;        /* ^ */
        /* UI state - domain of ui.c */
        struct TermKey *tk;               /* How to get a key pressed */
        struct unibi_term *ut;            /* How to talk to unibilium about terminfo */
        int term_width;                   /* Width of controlling term, in cells */
        int term_height;                  /* Height of controlling term, in cells */
        int *dirty_line;                  /* What we need to touch on next redraw */
        int dirty_top;                    /* Whether we should redraw [all of] top */
        int dirty_playlist;               /* Whether we should redraw [all of] bottom */
        size_t playlist_offset;           /* Index of first playlist entry displayed */
        size_t playlist_hover_idx;        /* What entry in the playlist we're hovering over */
        size_t pos_mid_line;              /* The y-position of the middle line */
        size_t pos_hints_top;             /* The y-position of the first UI hint line */
        int uri_or_chat;                  /* 0: show chat, and 1: show URIs */
};

/*
 * cytube.c (libwebsockets, site-specific stuff)
 */

/* Extract the ip-session cookie that sync demands */
int cytube_get_session_cookie(const char *server, const char *protocol, const
                              char *channel, struct state volatile *s);

/* Pass setup connection info (do this first) */
void cytube_set_cci_and_s(struct lws_client_connect_info *cci, struct state
                          volatile *sp);

/*
 * launch.c (mpv/youtube-dl spawning)
 */
void launch_downloader(const char *uri);

void launch_player(const char *uri);

/*
 * state.c (also includes some cytube-specific handling of emitted events)
 */
void state_clean(struct state volatile *s);
int state_handle(struct state volatile *s, const char *msg, size_t len);

/*
 * ui.c
 */
int ui_init(struct state volatile *s);
void ui_loop(struct state volatile *s);

/*
 * util.c
 */

/* snprintf + malloc + sprintf */
char * aprintf(const char *format, ...);

/* strlen, but with wcwidth in the middle */
int strwidth(const char *s);
