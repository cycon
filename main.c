/*
 * Copyright (c) 2019, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <locale.h>
#include <stdio.h>
#include <unistd.h>

#include <libwebsockets.h>
#include <pthread.h>

#include "cycon.h"
#include "macros.h"

const char *program_name = "cycon";

/* Usage message */
static void
usage(void)
{
        int len = strlen(program_name) + 7;

        fprintf(stderr, "usage: %s [ -s SERVER ] # like \"cytu.be\"\n",
                program_name);
        fprintf(stderr, "%*s [ -h ]        # if website is http (not https)\n",
                len, "");
        fprintf(stderr, "%*s -c CHANNEL\n", len, "");
}

/* LWS handling thread */
extern int cytube_lws_handler(struct lws *wsi, enum lws_callback_reasons reason,
                              void *user, void *in, size_t len);
void *
lws_spin(void *arg)
{
        int lret = 0;
        time_t now = 0;
        struct state volatile *s = (struct state volatile *) arg;
        char *ripped_host = 0;
        const struct lws_protocols p[] = {
                /* */
                {
                        /* */
                        .name = "sync",                                /* */
                        .callback = cytube_lws_handler,                /* */
                        .per_session_data_size = sizeof(struct state), /* */
                        .rx_buffer_size = 0,                           /* */
                        .id = 0,                                       /* */
                        .user = (void *) s,                            /* */
                        .tx_packet_size = 0,                           /* */
                }, { 0 },
        };
        struct lws_context_creation_info info = {
                /* */
                .options = LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT, /* */
                .port = CONTEXT_PORT_NO_LISTEN,                  /* */
                .protocols = p,                                  /* */
        };
        struct lws_context *context = 0;
        struct lws *wsi = 0;
        int sslf = s->https ? LCCSCF_USE_SSL | LCCSCF_ALLOW_SELFSIGNED : 0;
        struct lws_client_connect_info cci = {
                /* */
                .context = 0,               /* */
                .ssl_connection = sslf,     /* */
                .address = s->address,      /* */
                .origin = s->address,       /* */
                .protocol = "sync",         /* */
                .userdata = (void *) &s,    /* */
                .pwsi = &wsi,               /* */
        };

        /* First, HTTP stuff in preparation for LWS */
        if (cytube_get_session_cookie(cci.address, (s->https ? "https" :
                                                    "http"), s->channel_name,
                                      s) < 0) {
                ERROR_MESSAGE("cannot extract ip-session cookie");
                ERROR_MESSAGE("perhaps \"%s\" is not a channel?",
                              s->channel_name);
                s->please_die = 1;
                pthread_exit(0);
        }

        /* See cb_get_real_server */
        if (s->https) {
                ripped_host = strdup(s->socket_host + 8);
        } else {
                ripped_host = strdup(s->socket_host + 7);
        }

        if (!(ripped_host)) {
                goto done;
        }

        for (char *p = ripped_host; *p; ++p) {
                if (*p == ':') {
                        *p = 0;
                        cci.port = strtoll(p + 1, 0, 0);
                        break;
                }
        }

        cci.host = ripped_host;
        cci.address = ripped_host;
        lws_set_log_level(0, 0);
        cytube_set_cci_and_s(&cci, s);

        if (!(context = lws_create_context(&info))) {
                ERROR_MESSAGE("lws_create_context failed");
                s->please_die = 1;
                pthread_exit(0);
        }

        cci.context = context;
        s->last_playlist_req = time(0) + 120;

        while (!s->please_die) {
                if ((lret = lws_service(context, 1000)) < 0) {
                        break;
                }

                /* Do we need to ping? */
                now = time(0);

                if (s->established) {
                        if (s->last_ping + (s->ping_interval / 1000) <= now) {
                                s->must_write_ping = 1;
                                s->last_ping = now;
                        }

                        if (s->last_playlist_req + 120 <= now) {
                                s->must_ask_for_playlist = 1;
                                s->last_playlist_req = now;
                        }
                }

                /* Tell LWS we want to write */
                if (s->must_write_upgrade ||
                    s->must_write_ping ||
                    s->must_join_channel ||
                    s->must_ask_for_playlist) {
                        lws_callback_on_writable(wsi);
                }
        }

        lws_context_destroy(context);
done:
        free(ripped_host);

        return 0;
}

/* Main method */
int
main(int argc, char **argv)
{
        int ret = -1;
        struct state s = {
                /* */
                .address = "cytu.be", .https = 1,
        };
        char *server_arg = 0;
        char *channel_arg = 0;
        int opt = 0;
        pthread_t lws_thread = { 0 };

        setlocale(LC_ALL, "");

        /* Parse arguments */
        while ((opt = getopt(argc, argv, "s:c:h")) != -1) {
                switch (opt) {
                case 's':
                        server_arg = optarg;
                        break;
                case 'c':
                        channel_arg = optarg;
                        break;
                case 'h':
                        s.https = 0;
                        break;
                default:
                        usage();

                        return -1;
                }
        }

        if (server_arg) {
                s.address = server_arg;
        }

        if (!channel_arg) {
                usage();
                goto done;
        }

        s.channel_name = channel_arg;
        s.playlist.current_playing_uid = -1;

        if (ui_init(&s) < 0) {
                PERROR_MESSAGE("ui initialization error");
                goto done;
        }

        if (pthread_create(&lws_thread, 0, lws_spin, (void *) &s)) {
                PERROR_MESSAGE("pthread_create");
                goto done;
        }

        ui_loop(&s);
        s.please_die = 1;
        ret = 0;
        pthread_join(lws_thread, 0);
done:
        state_clean(&s);

        return ret;
}
