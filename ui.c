/*
 * Copyright (c) 2019, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <wchar.h>

#include <sys/ioctl.h>

#include <termkey.h>

#include <unibilium.h>

#include "cycon.h"
#include "macros.h"

#define xmin(a, b) ((a) < (b) ? (a) : (b))
#define xmax(a, b) ((a) > (b) ? (a) : (b))

#define FG "\x1b[38;2;%d;%d;%dm"
#define BG "\x1b[48;2;%d;%d;%dm"

/* Resize handling */
static volatile sig_atomic_t sigwinch_seen;

/* (unibi_var_t) { 0 } is too long */
static unibi_var_t Z;

/* Printf in format suitable for unibi_format() */
static void
out_printf(void *ctx, const char *buf, size_t len)
{
        ssize_t wout = 0;
        size_t wtot = 0;

        UNUSED(ctx);

        while (wtot < len) {
                if ((wout = write(1, buf + wtot, len - wtot)) < 0) {

                        /* An error that would be a pain in the neck to extract */
                        return;
                }

                wtot += wout;
        }
}

/* Wrapper for "Just execute the damn command" */
static void
do_unibi(unibi_term *ut, enum unibi_string which, unibi_var_t v1, unibi_var_t
         v2, unibi_var_t v3, unibi_var_t v4, unibi_var_t v5, unibi_var_t v6,
         unibi_var_t
         v7, unibi_var_t v8, unibi_var_t v9)
{
        unibi_var_t param[9] = { v1, v2, v3, v4, v5, v6, v7, v8, v9 };
        unibi_var_t zero_dyn[26] = { 0 };
        unibi_var_t zero_static[26] = { 0 };
        const char *format_string = unibi_get_str(ut, which);

        if (!format_string) {
                return;
        }

        unibi_format(zero_dyn, zero_static, format_string, param, out_printf, 0,
                     0, 0);
}

/* Write hh:mm:ss / hh:mm:ss */
static void
print_timing(struct playlist volatile *p)
{
        int play = p->current_playtime;
        int length = p->current_length;
        int cur_s = 0;
        int cur_m = 0;
        int cur_h = 0;
        int len_s = 0;
        int len_m = 0;
        int len_h = 0;

        if (p->paused) {
                printf("═══════ paused ══════");

                return;
        }

        cur_s = play % 60;
        play /= 60;
        cur_m = play % 60;
        play /= 60;
        cur_h = play % 100;
        len_s = length % 60;
        length /= 60;
        len_m = length % 60;
        length /= 60;
        len_h = length % 100;

        if (cur_h) {
                printf(" %02d:", cur_h);
        } else {
                printf("    ");
        }

        printf("%02d:%02d / ", cur_m, cur_s);

        if (len_h ||
            cur_h) {
                printf("%02d:", len_h);
        } else {
                printf("   ");
        }

        printf("%02d:%02d ", len_m, len_s);
}

/* Print out text, with left indentation. */
static void
trickle_out(struct state volatile *s, int left_pad, const char *text, int row)
{
        mbstate_t mbs = { 0 };
        size_t left = 0;
        wchar_t wc = 0;
        size_t mbret = 0;
        int cur_pos = left_pad;
        int max_width = s->term_width;

        if (!text) {
                return;
        }

        left = strlen(text);

        while (*text) {
                int print_num = 0;
                int this_cells = 0;

                mbret = mbrtowc(&wc, text, left, &mbs);

                if (mbret >= (size_t) -2) {
                        mbs = (mbstate_t) { 0 };
                        print_num = 1;
                        this_cells = 2;
                        continue;
                } else {
                        print_num = (int) mbret;
                        this_cells = wcwidth(wc);
                }

                if (this_cells + cur_pos > max_width) {
                        fflush(stdout);
                        do_unibi(s->ut, unibi_clr_eol, Z, Z, Z, Z, Z, Z, Z, Z,
                                 Z);
                        fflush(stdout);
                        row++;

                        if (row >= (int) s->pos_mid_line) {
                                return;
                        }

                        do_unibi(s->ut, unibi_cursor_address,
                                 unibi_var_from_num(row), Z, Z, Z, Z, Z, Z, Z,
                                 Z);
                        printf("%*s%.*s", left_pad, "", print_num, text);
                        cur_pos = left_pad + this_cells;
                } else {
                        printf("%.*s", print_num, text);
                }

                text += print_num;
                cur_pos += this_cells;
        }

        fflush(stdout);
}

/* Blit the whole damn thing */
static void
redraw(struct state volatile *s)
{
        /* Come off it */
        if (!s ||
            s->term_height < 5) {
                return;
        }

        /* First, the upper pane */
        if (!s->dirty_top) {
                goto top_drawn;
        }

        printf("\x1b[0m");
        fflush(stdout);

        if (s->server_error) {
                size_t error_len = strlen(s->server_error);
                int lines = (error_len / s->term_width) + 1;
                int error_start_pos = xmax(0, ((intmax_t) s->pos_mid_line -
                                               lines) / 2);

                for (size_t k = 0; k < s->pos_mid_line; k++) {
                        do_unibi(s->ut, unibi_cursor_address,
                                 unibi_var_from_num(k), Z, Z, Z, Z, Z, Z, Z, Z);
                        do_unibi(s->ut, unibi_clr_eol, Z, Z, Z, Z, Z, Z, Z, Z,
                                 Z);
                }

                do_unibi(s->ut, unibi_cursor_address, unibi_var_from_num(
                                 error_start_pos), Z, Z, Z, Z, Z, Z, Z, Z);
                printf("%s", s->server_error);
                fflush(stdout);
        } else if (s->uri_or_chat) {
                const char *u = 0;
                size_t idx = s->playlist_hover_idx;

                if (idx < s->playlist.entries_len) {
                        u = s->playlist.entries[idx].uri;
                } else {
                        u = " [ No media currently selected ]";
                }

                size_t uri_len = u ? strlen(u) : 0;
                int lines = (uri_len / s->term_width) + 1;
                int uri_start_pos = xmax(0, ((intmax_t) s->pos_mid_line -
                                             lines) / 2);

                for (size_t k = 0; k < s->pos_mid_line; k++) {
                        do_unibi(s->ut, unibi_cursor_address,
                                 unibi_var_from_num(k), Z, Z, Z, Z, Z, Z, Z, Z);
                        do_unibi(s->ut, unibi_clr_eol, Z, Z, Z, Z, Z, Z, Z, Z,
                                 Z);
                }

                do_unibi(s->ut, unibi_cursor_address, unibi_var_from_num(
                                 uri_start_pos), Z, Z, Z, Z, Z, Z, Z, Z);
                printf("%s", u);
                fflush(stdout);
        } else {
                if (s->term_width < 20) {
                        goto top_drawn;
                }

                intmax_t k = s->chat_log.msgs_len - 1;
                intmax_t pos = s->pos_mid_line - 1;
                size_t un_len = 0;
                int lines = 0;
                size_t text_width = 0;
                struct chat_msg *m = 0;

another_chat_msg:

                if (pos < 0 ||
                    pos > s->term_height) {
                        /* We probably wrapped to -1 somehow */
                        goto top_drawn;
                }

                if (k < 0 ||
                    k >= (int) s->chat_log.msgs_len) {
                        /* We wrapped to -1 */
                        do_unibi(s->ut, unibi_cursor_address,
                                 unibi_var_from_num(pos), Z, Z, Z, Z, Z, Z, Z,
                                 Z);
                        do_unibi(s->ut, unibi_clr_eol, Z, Z, Z, Z, Z, Z, Z, Z,
                                 Z);
                        fflush(stdout);
                        pos--;
                        goto another_chat_msg;
                }

                m = &s->chat_log.msgs[k];
                text_width = strwidth(m->text);
                un_len = strlen(m->user);
                un_len = xmax(un_len, 14) + 2;
                lines = 1 + (xmax(text_width - 1, 0) / (s->term_width -
                                                        un_len));

                if (lines - 1 > pos) {
                        for (int j = 0; j <= pos; ++j) {
                                do_unibi(s->ut, unibi_cursor_address,
                                         unibi_var_from_num(j), Z, Z, Z, Z, Z,
                                         Z, Z, Z);
                                do_unibi(s->ut, unibi_clr_eol, Z, Z, Z, Z, Z, Z,
                                         Z, Z, Z);
                                fflush(stdout);
                        }

                        goto top_drawn;
                } else {
                        do_unibi(s->ut, unibi_cursor_address,
                                 unibi_var_from_num(pos - lines + 1), Z, Z, Z,
                                 Z, Z, Z,
                                 Z, Z);
                        printf("%*s: ", 14, m->user);
                        fflush(stdout);
                        trickle_out(s, un_len, m->text, pos - lines + 1);
                        do_unibi(s->ut, unibi_clr_eol, Z, Z, Z, Z, Z, Z, Z, Z,
                                 Z);
                        fflush(stdout);
                }

                k--;
                pos -= lines;
                goto another_chat_msg;
        }

top_drawn:

        /* The middle line */
        if (s->dirty_line[s->pos_mid_line]) {
                do_unibi(s->ut, unibi_cursor_address, unibi_var_from_num(
                                 s->pos_mid_line), Z, Z, Z, Z, Z, Z, Z, Z);

                for (int k = 0; k < s->term_width; ++k) {
                        if (k == 10) {
                                printf("╤");
                        } else {
                                printf("═");
                        }
                }
        }

        s->dirty_top = 0;
        fflush(stdout);

        if (s->dirty_playlist) {
                for (size_t k = s->pos_mid_line + 1; k <
                     (size_t) s->term_height; ++k) {
                        s->dirty_line[k] = 1;
                }

                s->dirty_playlist = 0;
        }

        /* Now the bottom part */
        for (size_t k = 0; s->pos_mid_line + 1 + k < (size_t) s->pos_hints_top;
             ++k) {
                int pos = s->pos_mid_line + 1 + k;
                size_t idx = s->playlist_offset + k;

                if (!s->dirty_line[pos]) {
                        continue;
                }

                do_unibi(s->ut, unibi_cursor_address, unibi_var_from_num(pos),
                         Z, Z, Z, Z, Z, Z, Z, Z);
                const char *divider = " │ ";
                const char *runtime = "";
                const char *title = "";
                int title_width = 0;
                int r = 0;

                if (idx < s->playlist.entries_len) {
                        struct playlist_entry *e = &s->playlist.entries[idx];

                        runtime = e->runtime ? e->runtime : "";
                        title = e->title ? e->title : "";
                        title_width = e->title_width;

                        if (s->playlist.current_playing_uid ==
                            s->playlist.entries[idx].uid) {
                                /* divider = "━┿━"; */
                                divider = " ┿ ";
                        }
                }

                if (s->playlist_hover_idx == idx) {
                        printf("\x1b[48;2;%d;%d;%dm", 34, 54, 69);
                } else {
                        printf("\x1b[48;2;%d;%d;%dm", 40, 40, 40);
                }

                if (s->term_width > 1 + 8 + 3) {
                        printf(" %*s%s%s", 8, runtime, divider, title);
                        r = 1 + 8 + 3 + title_width;
                }

                while (r < s->term_width) {
                        putchar(' ');
                        r++;
                }

                fflush(stdout);
        }

        printf("\x1b[0m");
        fflush(stdout);

        /* The bottom line */
        if (s->dirty_line[s->term_height - 3]) {
                do_unibi(s->ut, unibi_cursor_address, unibi_var_from_num(
                                 s->term_height - 3), Z, Z, Z, Z, Z, Z, Z, Z);
                printf("\x1b[48;2;%d;%d;%dm", 40, 40, 40);

                for (int k = 0; k < s->term_width; ++k) {
                        if (k == 10) {
                                printf("╧");
                        } else if (k == 15 &&
                                   s->term_width > 35 &&
                                   s->playlist.current_playing_uid >= 0) {
                                print_timing(&s->playlist);
                                k = 35;
                        } else {
                                printf("═");
                        }
                }

                fflush(stdout);
        }

        /* Bottom text */
        if (!s->dirty_line[s->term_height - 2] &&
            !s->dirty_line[s->term_height - 1]) {
                goto all_done;
        }

        do_unibi(s->ut, unibi_cursor_address, unibi_var_from_num(
                         s->term_height - 2), Z, Z, Z, Z, Z, Z, Z, Z);
        printf(BG " J " BG ": scroll down          ", 34, 54, 69, 40, 40, 40);
        printf(BG " V " BG ": mpv {selected}       ", 34, 54, 69, 40, 40, 40);

        if (s->uri_or_chat) {
                printf(BG " C " BG ": view chat", 34, 54, 69, 40, 40, 40);
        } else {
                printf(BG " U " BG ": view URIs", 34, 54, 69, 40, 40, 40);
        }

        for (int r = 66; r < s->term_width; ++r) {
                putchar(' ');
        }

        printf("\x1b[0m");
        fflush(stdout);
        do_unibi(s->ut, unibi_cursor_address, unibi_var_from_num(
                         s->term_height - 1), Z, Z, Z, Z, Z, Z, Z, Z);
        printf(BG " K " BG ": scroll up            ", 34, 54, 69, 40, 40, 40);
        printf(BG " D " BG ": download {selected}  ", 34, 54, 69, 40, 40, 40);
        printf(BG " Q " BG ": quit", 34, 54, 69, 40, 40, 40);

        for (int r = 61; r < s->term_width; ++r) {
                putchar(' ');
        }

        printf("\x1b[0m");
        fflush(stdout);
all_done:

        for (int k = 0; k < s->term_height; ++k) {
                s->dirty_line[k] = 0;
        }
}

/* Move the hovered line up and down */
static void
move_playlist_hover(struct state volatile *s, int delta)
{
        size_t old_hover_pos = s->pos_mid_line + 1 + s->playlist_hover_idx -
                               s->playlist_offset;
        size_t new_hover_idx = (size_t) xmax(0, xmin(
                                                     (intmax_t) s->
                                                     playlist_hover_idx + delta,
                                                     (intmax_t) s->playlist.
                                                     entries_len - 1));
        size_t new_hover_pos = s->pos_mid_line + 1 + new_hover_idx -
                               s->playlist_offset;
        size_t highest_cutoff = s->pos_mid_line + 4;

        if (old_hover_pos >= (size_t) s->term_height) {
                old_hover_pos = xmax(0, s->term_height - 1);
        }

        if (new_hover_pos >= (size_t) s->term_height) {
                new_hover_pos = xmax(0, s->term_height - 1);
        }

        s->dirty_line[old_hover_pos] = 1;
        s->dirty_line[new_hover_pos] = 1;

        if (s->uri_or_chat) {
                s->dirty_top = 1;
        }

        s->playlist_hover_idx = new_hover_idx;

        while (highest_cutoff > new_hover_pos &&
               s->playlist_offset > 0) {
                s->playlist_offset--;
                new_hover_pos++;
                s->dirty_playlist = 1;
        }

        while (new_hover_pos + 4 > s->pos_hints_top) {
                s->playlist_offset++;
                new_hover_pos--;
                s->dirty_playlist = 1;
        }
}

/* All the input things we can do */
static void
handle_keypress(struct state volatile *s, TermKeyKey *k)
{
        switch (k->type) {
        case TERMKEY_TYPE_KEYSYM:

                switch (k->code.sym) {
                case TERMKEY_SYM_UP:
                        move_playlist_hover(s, -1);
                        break;
                case TERMKEY_SYM_DOWN:
                        move_playlist_hover(s, 1);
                        break;
                default:
                        break;
                }

                break;
        case TERMKEY_TYPE_UNICODE:

                switch (k->code.codepoint) {
                case 0x0063: /* c */
                case 0x0043:
                        s->uri_or_chat = 0;
                        s->dirty_top = 1;
                        s->dirty_playlist = 1;
                        break;
                case 0x0075: /* u */
                case 0x0055:
                        s->uri_or_chat = 1;
                        s->dirty_top = 1;
                        s->dirty_playlist = 1;
                        break;
                case 0x006a: /* j */
                case 0x004a:
                        move_playlist_hover(s, 1);
                        break;
                case 0x006b: /* k */
                case 0x004b:
                        move_playlist_hover(s, -1);
                        break;
                case 0x006c: /* l */
                case 0x004c:
                        s->dirty_top = 1;
                        s->dirty_playlist = 1;

                        for (int q = 0; q < s->term_height; ++q) {
                                s->dirty_line[q] = 1;
                        }

                        break;
                case 0x0071: /* q */
                case 0x0051:
                        s->please_die = 1;
                        break;
                case 0x0064: /* d */
                case 0x0044:

                        if (s->playlist_hover_idx < s->playlist.entries_len) {
                                struct playlist_entry *e =
                                        &s->playlist.entries[s->
                                                             playlist_hover_idx];

                                if (e->uri) {
                                        launch_downloader(e->uri);
                                }
                        }

                        break;
                case 0x0076: /* v */
                case 0x0056:

                        if (s->playlist_hover_idx < s->playlist.entries_len) {
                                struct playlist_entry *e =
                                        &s->playlist.entries[s->
                                                             playlist_hover_idx];

                                if (e->uri) {
                                        launch_player(e->uri);
                                }
                        }

                        break;
                default:
                        break;
                }

                break;
        default:
                break;
        }
}

/* Make sure we don't waste too much space on empty playlists */
static void
shrinkwrap_playlist(struct state volatile *s)
{
        size_t hypothetical_pos_mid_line = 1 + (s->term_height - 2) / 3;
        size_t new_pos_mid_line = s->pos_mid_line;

        /*
         * Jumps around a bit too much for now, so the core of the
         * function is commented out.
         */

        /*
           if (s->pos_hints_top > s->pos_mid_line + s->playlist.entries_len + 2) {
                new_pos_mid_line = s->pos_hints_top - s->playlist.entries_len -
                                   2;
           }
         */
        new_pos_mid_line = xmax(hypothetical_pos_mid_line, new_pos_mid_line);

        if (new_pos_mid_line != s->pos_mid_line) {
                s->pos_mid_line = new_pos_mid_line;

                for (size_t k = 0; k < (size_t) s->term_height; ++k) {
                        s->dirty_line[k] = 1;
                }

                s->dirty_top = 1;
        }
}

/* Terminal has changed under us */
static void
handle_resize(struct state volatile *s)
{
        struct winsize w = { 0 };
        int *newmem = 0;
        size_t n_rows = 0;

        ioctl(1, TIOCGWINSZ, &w);
        n_rows = w.ws_row;

        if (n_rows < 8) {
                n_rows = 8;
        }

        if (!(newmem = malloc(n_rows * sizeof(*s->dirty_line)))) {
                PERROR_MESSAGE("malloc");
                s->please_die = 1;

                return;
        }

        s->term_width = w.ws_col;
        s->term_height = w.ws_row;
        free(s->dirty_line);
        s->dirty_line = newmem;
        memset(s->dirty_line, 1, s->term_height * sizeof(*s->dirty_line));
        s->dirty_top = 1;
        s->pos_mid_line = 1 + (s->term_height - 2) / 3;
        s->pos_mid_line = (size_t) xmax(0, xmin((intmax_t) s->pos_mid_line,
                                                s->term_height - 1));
        s->pos_hints_top = (size_t) xmax(0, s->term_height - 3);
        shrinkwrap_playlist(s);
        move_playlist_hover(s, 0);
}

/* See a SIGWINCH */
static void
sighandle_winch(int sig)
{
        UNUSED(sig);
        sigwinch_seen = 1;
}

/* Clean out our part of s, prepare to surrender terminal. */
static void
ui_teardown(struct state volatile *s)
{
        free(s->dirty_line);
        s->dirty_line = 0;

        /* termkey */
        termkey_destroy(s->tk);
        s->tk = 0;

        /* unibilium */
        do_unibi(s->ut, unibi_cursor_normal, Z, Z, Z, Z, Z, Z, Z, Z, Z);
        do_unibi(s->ut, unibi_exit_attribute_mode, Z, Z, Z, Z, Z, Z, Z, Z, Z);
        do_unibi(s->ut, unibi_exit_ca_mode, Z, Z, Z, Z, Z, Z, Z, Z, Z);
        unibi_destroy(s->ut);
        s->ut = 0;
}

/* Acquire terminal, save some data */
int
ui_init(struct state volatile *s)
{
        if (!isatty(fileno(stdin)) ||
            !isatty(fileno(stdout))) {
                ERROR_MESSAGE("stdin and/or stdout are not terminals");
                ERROR_MESSAGE("[ If you really, really want to go    ]");
                ERROR_MESSAGE("[ ahead, this check is the only place ]");
                ERROR_MESSAGE("[ in the program that cares.          ]");
                s->please_die = 1;

                return -1;
        }

        /* Resize handling */
        signal(SIGWINCH, sighandle_winch);

        /* unibilium */
        s->ut = unibi_from_env();
        do_unibi(s->ut, unibi_enter_ca_mode, Z, Z, Z, Z, Z, Z, Z, Z, Z);
        do_unibi(s->ut, unibi_keypad_xmit, Z, Z, Z, Z, Z, Z, Z, Z, Z);
        do_unibi(s->ut, unibi_cursor_invisible, Z, Z, Z, Z, Z, Z, Z, Z, Z);
        do_unibi(s->ut, unibi_clear_screen, Z, Z, Z, Z, Z, Z, Z, Z, Z);

        /* termkey */
        s->tk = termkey_new(fileno(stdin), 0);
        termkey_set_waittime(s->tk, 100);

        return 0;
}

void
ui_loop(struct state volatile *s)
{
        int sv_errno = 0;
        TermKeyResult ret;
        TermKeyKey key;
        int new_playtime = 0;
        struct pollfd pfd = { .fd = fileno(stdin), .events = POLLIN };

        sigwinch_seen = 1;

        while (!s->please_die) {
                if (sigwinch_seen) {
                        sigwinch_seen = 0;
                        handle_resize(s);
                }

                if (!s->playlist.paused) {
                        new_playtime = s->playlist.zero_playtime + (time(0) -
                                                                    s->playlist.
                                                                    zero_clocktime);

                        if (new_playtime != s->playlist.current_playtime) {
                                s->dirty_line[s->pos_hints_top] = 1;
                        }

                        if (new_playtime > s->playlist.current_length + 2) {
                                s->playlist.current_playing_uid = -1;
                                s->playlist.current_playtime = 0;
                                s->playlist.current_length = 0;
                        }

                        s->playlist.current_playtime = new_playtime;
                }

                redraw(s);

                if (poll(&pfd, 1, 100) == 0) {
                        ret = termkey_getkey_force(s->tk, &key);
                        goto check_key;
                }

                if (pfd.revents & (POLLIN | POLLHUP | POLLERR)) {
                        termkey_advisereadable(s->tk);
                }

                ret = termkey_getkey(s->tk, &key);
check_key:

                switch (ret) {
                case TERMKEY_RES_NONE:
                case TERMKEY_RES_AGAIN:
                        break;
                case TERMKEY_RES_ERROR:
                        sv_errno = errno;
                        ui_teardown(s);
                        errno = sv_errno;
                        PERROR_MESSAGE("termkey_waitkey");

                        return;
                case TERMKEY_RES_EOF:
                        goto done;
                case TERMKEY_RES_KEY:
                        handle_keypress(s, &key);
                        break;
                }
        }

done:
        ui_teardown(s);
}
