/*
 * Copyright (c) 2019, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <limits.h>
#include <stdarg.h>
#include <stdlib.h>

#include <curl/curl.h>
#include <libwebsockets.h>
#include <yajl/yajl_tree.h>

#include "cycon.h"
#include "macros.h"

/* How to connect */
static struct lws_client_connect_info *cci;

/* LWS userdata is insane, like everything else. Screw it */
struct state volatile *main_s;

/* Hurrah, another layer of indirection */
struct real_server_ud {
        /* */
        struct state volatile *s;
        size_t chunk_len;
        char *chunk;
};

/* Do nothing */
static size_t
curl_drop(char *buf, size_t size, size_t nmemb, void *userdata)
{
        UNUSED(buf);
        UNUSED(userdata);

        return size * nmemb;
}

/* Rip sid out of a JSON-esque reply */
static size_t
cb_extract_sid_and_ping(char *buf, size_t size, size_t nmemb, void *userdata)
{
        size_t ret = 0;
        size_t len = size * nmemb;
        char *sid_start = 0;
        size_t sid_off = 0;
        size_t sid_len = 0;
        char *pingInt_start = 0;
        size_t pingInt_off = 0;
        struct state volatile *s = (struct state volatile *) userdata;

        if (len / size != nmemb) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        for (sid_off = 0; sid_off + 7 < len; ++sid_off) {
                if (!strncmp(buf + sid_off, "\"sid\":\"", 7)) {
                        sid_off += 7;
                        sid_start = buf + sid_off;
                        break;
                }
        }

        if (!sid_start) {
                goto wellfine;
        }

        for (sid_len = 0; sid_off + sid_len < len; ++sid_len) {
                if (sid_start[sid_len] == '"') {
                        if (!(s->sid = strndup(sid_start, sid_len))) {
                                PERROR_MESSAGE("strndup");
                                goto done;
                        }

                        break;
                }
        }

        for (pingInt_off = 0; pingInt_off + 15 < len; ++pingInt_off) {
                if (!strncmp(buf + pingInt_off, "\"pingInterval\":", 15)) {
                        pingInt_off += 15;
                        pingInt_start = buf + pingInt_off;
                }
        }

        if (!pingInt_start) {
                goto wellfine;
        }

        s->ping_interval = strtoll(pingInt_start, 0, 10);
wellfine:
        ret = len;
done:

        return ret;
}

/* snprintf + malloc + sprintf */
int
perform_lws_write(struct lws *wsi, const char *format, ...)
{
        int ret = -1;
        va_list arglist;
        char *mem;
        size_t len = 0;

        va_start(arglist, format);
        len = vsnprintf(0, 0, format, arglist);
        va_end(arglist);

        if (!(mem = malloc(len + LWS_PRE + 1))) {
                goto done;
        }

        va_start(arglist, format);
        vsprintf(mem + LWS_PRE, format, arglist);
        va_end(arglist);
        lws_write(wsi, (unsigned char *) (mem + LWS_PRE), len, LWS_WRITE_TEXT);
        free(mem);
        ret = 0;
done:

        return ret;
}

/* Follow the breadcrumb trail of servers */
static size_t
cb_get_real_server(char *buf, size_t size, size_t nmemb, void *userdata)
{
        size_t ret = 0;
        size_t len = size * nmemb;
        struct real_server_ud *rsu = (struct real_server_ud *) userdata;
        size_t old_len = rsu->chunk_len;
        size_t new_len = 0;
        void *newmem = 0;
        yajl_val tree = 0;
        yajl_val servers = 0;
        char eb[1024] = { 0 };
        const char *url_path[] = { "url", 0 };
        const char *servers_path[] = { "servers", 0 };

        if (len / size != nmemb) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        if (SSIZE_MAX - len - 1 < rsu->chunk_len) {
                ERROR_MESSAGE("overflow");
                goto done;
        }

        new_len = rsu->chunk_len + len;

        if (!(newmem = realloc(rsu->chunk, new_len + 1))) {
                PERROR_MESSAGE("malloc");
                goto done;
        }

        rsu->chunk = newmem;
        rsu->chunk_len = new_len;
        memcpy(rsu->chunk + old_len, buf, len);
        rsu->chunk[new_len] = 0;
        ret = len;

        /* See handle_state: we're trying to save some memleaks in yajl */
        if (rsu->chunk[new_len - 1] != '}') {
                goto done;
        }

        tree = yajl_tree_parse(rsu->chunk, eb, 1024);

        if (eb[0]) {
                goto done;
        }

        if (!tree ||
            !(servers = yajl_tree_get(tree, servers_path, yajl_t_array))) {
                goto done;
        }

        if (!(YAJL_IS_ARRAY(servers))) {
                goto done;
        }

        for (size_t k = 0; k < YAJL_GET_ARRAY(servers)->len; ++k) {
                yajl_val e = YAJL_GET_ARRAY(servers)->values[k];
                const char *url = YAJL_GET_STRING(yajl_tree_get(e, url_path,
                                                                yajl_t_string));

                if (!url) {
                        continue;
                }

                if ((rsu->s->https &&
                     !strncmp(url, "https://", 8)) ||
                    (!rsu->s->https &&
                     !strncmp(url, "http://", 7))) {
                        free(rsu->s->socket_host);
                        rsu->s->socket_host = 0;

                        if (!(rsu->s->socket_host = strdup(url))) {
                                return 0;
                        }

                        goto done;
                }
        }

done:
        yajl_tree_free(tree);

        return ret;
}

/* Extract the session id cookie that sync demands */
#define BUF_LEN 1024
int
cytube_get_session_cookie(const char *server, const char *protocol, const
                          char *channel, struct state volatile *s)
{
        /*
         * Grumble grumble -- we only need libcurl for one request,
         * to get the json file, and we *could* parse that
         * ourselves except for the fact that everything redirects
         * to https and *ssl is a pita.
         */
        int ret = -1;
        char *url = 0;
        char curl_error[CURL_ERROR_SIZE];
        CURL *ch;
        struct real_server_ud rsu = { .s = s };

        curl_global_init(0);

        if (!(ch = curl_easy_init())) {
                ERROR_MESSAGE("curl_easy_init failed");
                goto done;
        }

        /* Grab real connection info */
        if (!(url = aprintf("%s://%s/socketconfig/%s.json", protocol, server,
                            channel))) {
                PERROR_MESSAGE("aprintf");
                goto done;
        }

        curl_easy_setopt(ch, CURLOPT_URL, url);
        curl_easy_setopt(ch, CURLOPT_NOBODY, 0L);
        curl_easy_setopt(ch, CURLOPT_FOLLOWLOCATION, 2L);
        curl_easy_setopt(ch, CURLOPT_MAXREDIRS, 2L);
        curl_easy_setopt(ch, CURLOPT_WRITEDATA, (void *) &rsu);
        curl_easy_setopt(ch, CURLOPT_WRITEFUNCTION, cb_get_real_server);
        curl_easy_setopt(ch, CURLOPT_HEADERFUNCTION, curl_drop);
        curl_easy_setopt(ch, CURLOPT_ERRORBUFFER, &curl_error);

        if (curl_easy_perform(ch) != CURLE_OK) {
                ERROR_MESSAGE("curl_easy_perform: %s", curl_error);
                goto done;
        }

        free(rsu.chunk);
        rsu = (struct real_server_ud) { 0 };

        /* Now, connect to the socket.io thing to get the sid */
        free(url);

        if (!(url = aprintf("%s/socket.io/?EIO=3&transport=polling",
                            s->socket_host))) {
                PERROR_MESSAGE("aprintf");
                goto done;
        }

        curl_easy_setopt(ch, CURLOPT_URL, url);
        curl_easy_setopt(ch, CURLOPT_WRITEFUNCTION, cb_extract_sid_and_ping);
        curl_easy_setopt(ch, CURLOPT_WRITEDATA, (void *) s);

        if (curl_easy_perform(ch) != CURLE_OK) {
                ERROR_MESSAGE("curl_easy_perform: %s", curl_error);
                goto done;
        }

        if (!s->sid) {
                ERROR_MESSAGE("/socket.io did not return sid -- bailing");
                goto done;
        }

        if (s->ping_interval < 200) {
                s->ping_interval = 200;
        }

        ret = 0;
done:
        free(url);

        if (ch) {
                curl_easy_cleanup(ch);
        }

        curl_global_cleanup();

        return ret;
}

/* Deal with something to do with lws */
int
cytube_lws_handler(struct lws *wsi, enum lws_callback_reasons reason,
                   void *user, void *in, size_t len)
{
        const char *str = 0;
        char *tmp = 0;
        struct state volatile *s = main_s;

        UNUSED(user);

        if (!cci) {
                ERROR_MESSAGE("cci is somehow not set");
        }

        if (!s) {
                ERROR_MESSAGE("s is somehow not set");
        }

        switch (reason) {
        case LWS_CALLBACK_PROTOCOL_INIT:

                if (!(tmp = aprintf(
                              "/socket.io/?EIO=3&transport=websocket&sid=%s",
                              s->sid))) {
                        ERROR_MESSAGE("aprintf");

                        return 1;
                }

                cci->path = tmp;
                lws_client_connect_via_info(cci);
                cci->path = 0;
                free(tmp);
                break;
        case LWS_CALLBACK_CLIENT_APPEND_HANDSHAKE_HEADER:
                break;
        case LWS_CALLBACK_CLIENT_ESTABLISHED:

                /* "I'm in" */
                s->established = 1;
                s->must_write_upgrade = 1;
                s->must_join_channel = 1;
                lws_callback_on_writable(wsi);
                break;
        case LWS_CALLBACK_CLIENT_WRITEABLE:

                if (s->must_write_upgrade) {
                        /* I have no idea why we have to do this */
                        perform_lws_write(wsi, "5");
                        s->must_write_upgrade = 0;
                        break;
                } else if (s->must_write_ping) {
                        perform_lws_write(wsi, "2");
                        s->must_write_ping = 0;
                        break;
                } else if (s->must_join_channel) {
                        perform_lws_write(wsi,
                                          "42[\"joinChannel\", {\"name\": \"%s\"}]",
                                          s->channel_name);
                        s->must_join_channel = 0;

                        /* We have to wait until we've fully joined to ask for the playlist */
                        break;
                } else if (s->must_ask_for_playlist) {
                        perform_lws_write(wsi, "42[\"requestPlaylist\"]");
                        s->must_ask_for_playlist = 0;
                        break;
                }

                break;
        case LWS_CALLBACK_CLIENT_RECEIVE:
                str = (const char *) in;

                if ((len == 1 &&
                     str[0] == '3') ||
                    s->stored_cmd_len >= (1 << 25)) {
                        /* This is a PONG. Scrap whatever incompletes we've got */
                        free(s->stored_cmd);
                        s->stored_cmd = 0;
                        s->stored_cmd_len = 0;
                } else if (s->stored_cmd) {
                        /* We're continuing an incomplete message, right? */
                        void *newmem = 0;

                        if (!(newmem = realloc(s->stored_cmd,
                                               s->stored_cmd_len + len))) {
                                free(s->stored_cmd);
                                s->stored_cmd = 0;
                                s->stored_cmd_len = 0;
                        } else {
                                s->stored_cmd = newmem;
                                memcpy(s->stored_cmd + s->stored_cmd_len, str,
                                       len);
                                s->stored_cmd_len += len;

                                if (state_handle(s, s->stored_cmd,
                                                 s->stored_cmd_len) >= 0) {
                                        free(s->stored_cmd);
                                        s->stored_cmd = 0;
                                        s->stored_cmd_len = 0;
                                }
                        }
                } else if (len > 2 &&
                           str[0] == '4' &&
                           (str[1] == '2' ||
                            str[1] == '5')) {
                        if (state_handle(s, str + 2, len - 2) < 0) {
                                /* Perhaps this is the start of an incomplete message */
                                if ((s->stored_cmd = strndup(str + 2, len -
                                                             2))) {
                                        s->stored_cmd_len = len - 2;
                                }
                        }
                }

                /* HANDLE MESSAGES HERE */
                break;
        case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:
                ERROR_MESSAGE("LWS error: %*s", (int) len, in ? (char *) in :
                              "");
                s->please_die = 1;
                break;
        case LWS_CALLBACK_PROTOCOL_DESTROY:
        case LWS_CALLBACK_WSI_DESTROY:
                s->please_die = 1;
                break;
        case LWS_CALLBACK_OPENSSL_LOAD_EXTRA_CLIENT_VERIFY_CERTS:
        case LWS_CALLBACK_GET_THREAD_ID:
        case LWS_CALLBACK_ADD_POLL_FD:
        case LWS_CALLBACK_WSI_CREATE:
        case LWS_CALLBACK_EVENT_WAIT_CANCELLED:
        case LWS_CALLBACK_OPENSSL_PERFORM_SERVER_CERT_VERIFICATION:
        case LWS_CALLBACK_LOCK_POLL:
        case LWS_CALLBACK_UNLOCK_POLL:
        case LWS_CALLBACK_CHANGE_MODE_POLL_FD:
        case LWS_CALLBACK_DEL_POLL_FD:
        case LWS_CALLBACK_VHOST_CERT_AGING:
                break;
        default:

                /* We get 2 on startup (?) and 75 on destroy (?) */
                /* ERROR_MESSAGE("unknown lws response; reason = %d", reason); */
                break;
        }

        return 0;
}

/* Pass in the cci for use in handler */
void
cytube_set_cci_and_s(struct lws_client_connect_info *ccip, struct state
                     volatile *sp)
{
        cci = ccip;
        main_s = sp;
}
