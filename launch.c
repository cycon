/*
 * Copyright (c) 2019, De Rais <derais@cock.li>
 *
 * Permission to use, copy, modify, and/or distribute this software for
 * any purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <errno.h>
#include <spawn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "cycon.h"
#include "macros.h"

extern char **environ;

void
launch_downloader(const char *uri)
{
        pid_t dummy = 0;
        posix_spawn_file_actions_t fa;
        int kill_fa = 0;
        posix_spawnattr_t sa;
        int kill_sa = 0;
        char *uridup = strdup(uri);
        char *prog = strdup("youtube-dl");
        char *quiet = strdup("--quiet");
        char *no_warnings = strdup("--no-warnings");
        char * const argv[5] = { prog, quiet, no_warnings, uridup, 0 };

        if (!uridup ||
            !prog ||
            !quiet) {
                goto done;
        }

        if (posix_spawn_file_actions_init(&fa)) {
                PERROR_MESSAGE("posix_spawn_file_actions_init");
                goto done;
        }

        kill_fa = 1;

        if (posix_spawnattr_init(&sa)) {
                PERROR_MESSAGE("posix_spawnattr_init");
                goto done;
        }

        kill_sa = 1;

        if (posix_spawn_file_actions_addclose(&fa, 0)) {
                PERROR_MESSAGE("posix_spawn_file_actions_addclose");
                goto done;
        }

        if (posix_spawn_file_actions_addclose(&fa, 1)) {
                PERROR_MESSAGE("posix_spawn_file_actions_addclose");
                goto done;
        }

        if (posix_spawn_file_actions_addclose(&fa, 2)) {
                PERROR_MESSAGE("posix_spawn_file_actions_addclose");
                goto done;
        }

        if (posix_spawnattr_setflags(&sa, POSIX_SPAWN_SETSIGDEF)) {
                PERROR_MESSAGE("posix_spawnattr_setflags");
                goto done;
        }

        if (posix_spawn_file_actions_addopen(&fa, 0, "/dev/null", O_RDONLY,
                                             0)) {
                PERROR_MESSAGE("posix_spawn_file_actions_addopen");
                goto done;
        }

        if (posix_spawn_file_actions_addopen(&fa, 1, "/dev/null", O_WRONLY,
                                             0)) {
                PERROR_MESSAGE("posix_spawn_file_actions_addopen");
                goto done;
        }

        if (posix_spawn_file_actions_adddup2(&fa, 1, 2)) {
                PERROR_MESSAGE("posix_spawn_file_actions_adddup2");
                goto done;
        }

        posix_spawnp(&dummy, "youtube-dl", &fa, &sa, argv, environ);
done:

        if (kill_fa) {
                posix_spawn_file_actions_destroy(&fa);
        }

        if (kill_sa) {
                posix_spawnattr_destroy(&sa);
        }

        free(uridup);
        free(prog);
        free(quiet);
        free(no_warnings);
}

void
launch_player(const char *uri)
{
        pid_t dummy = 0;
        posix_spawn_file_actions_t fa;
        int kill_fa = 0;
        posix_spawnattr_t sa;
        int kill_sa = 0;
        char *uridup = strdup(uri);
        char *prog = strdup("mpv");
        char *quiet = strdup("--quiet");
        char * const argv[4] = { prog, quiet, uridup, 0 };

        if (!uridup ||
            !prog ||
            !quiet) {
                goto done;
        }

        if (posix_spawn_file_actions_init(&fa)) {
                PERROR_MESSAGE("posix_spawn_file_actions_init");
                goto done;
        }

        kill_fa = 1;

        if (posix_spawnattr_init(&sa)) {
                PERROR_MESSAGE("posix_spawnattr_init");
                goto done;
        }

        kill_sa = 1;

        if (posix_spawn_file_actions_addclose(&fa, 0)) {
                PERROR_MESSAGE("posix_spawn_file_actions_addclose");
                goto done;
        }

        if (posix_spawn_file_actions_addclose(&fa, 1)) {
                PERROR_MESSAGE("posix_spawn_file_actions_addclose");
                goto done;
        }

        if (posix_spawn_file_actions_addclose(&fa, 2)) {
                PERROR_MESSAGE("posix_spawn_file_actions_addclose");
                goto done;
        }

        if (posix_spawnattr_setflags(&sa, POSIX_SPAWN_SETSIGDEF)) {
                PERROR_MESSAGE("posix_spawnattr_setflags");
                goto done;
        }

        if (posix_spawn_file_actions_addopen(&fa, 0, "/dev/null", O_RDONLY,
                                             0)) {
                PERROR_MESSAGE("posix_spawn_file_actions_addopen");
                goto done;
        }

        if (posix_spawn_file_actions_addopen(&fa, 1, "/dev/null", O_WRONLY,
                                             0)) {
                PERROR_MESSAGE("posix_spawn_file_actions_addopen");
                goto done;
        }

        if (posix_spawn_file_actions_adddup2(&fa, 1, 2)) {
                PERROR_MESSAGE("posix_spawn_file_actions_adddup2");
                goto done;
        }

        posix_spawnp(&dummy, "mpv-with-load-screen", &fa, &sa, argv, environ);
done:

        if (kill_fa) {
                posix_spawn_file_actions_destroy(&fa);
        }

        if (kill_sa) {
                posix_spawnattr_destroy(&sa);
        }

        free(uridup);
        free(prog);
        free(quiet);
}
